﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shop.Api.Dto;
using Shop.Api.Services;

namespace Shop.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        public IProductService _productService { get; set; }
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<IActionResult> GetProduct()
        {
            return Ok(await _productService.GetProduct());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductById(int id)
        {
            return Ok(await _productService.GetProductById(id));
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateProduct(ProductDto dto)
        {
            return Ok(await _productService.CreateProduct(dto));
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProduct(ProductDto dto)
        {
            return Ok(await _productService.UpdateProduct(dto));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            return Ok(await _productService.DeleteProduct(id));
        }

    }
}
