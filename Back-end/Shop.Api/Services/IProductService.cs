﻿using Shop.Api.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Api.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> GetProduct();
        Task<ProductDto> GetProductById(int id);
        Task<ProductDto> CreateProduct(ProductDto dto);
        Task<ProductDto> UpdateProduct(ProductDto dto);
        Task<int> DeleteProduct(int id);
    }
}
