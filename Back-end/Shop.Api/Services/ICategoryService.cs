﻿using Shop.Api.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.Api.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<CategoryDto>> GetCategory();
    }
}
