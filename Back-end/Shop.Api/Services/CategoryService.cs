﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Shop.Api.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.Api.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IBoltGraphClient _graphClient;

        public CategoryService(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }

        public async Task<IEnumerable<CategoryDto>> GetCategory()
        {
            var query = _graphClient.Cypher.Read
                .Match($"(O:Category)")
                .ReturnDistinct((O, IT) => new CategoryDto()
                {
                    Id = Return.As<int>("id(O)"),
                    Name = Return.As<string>("O.Name")
                });

            var categories = await query.ResultsAsync;
            return categories;
        }
    }
}
