﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Shop.Api.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Shop.Api.Services
{
    public class ProductService : IProductService
    {
        private readonly IBoltGraphClient _graphClient;
        public ProductService(IBoltGraphClient graphClient)
        {
            _graphClient = graphClient;
        }
        public async Task<ProductDto> CreateProduct(ProductDto dto)
        {
            try
            {
                var query = _graphClient.Cypher.Write
                        .Create("(p:Product{Name:'" + dto.Name + "',Description:'" + dto.Description + "'})")
                        .With("p")
                        .Match($"(c:Category)").Where($"id(c)={dto.Category.Id}")
                        .Create("(c)-[:HAS_PRODUCT]->(p)")
                        .With("p,c")
                        .Return(_ => Return.As<ProductDto>(@"{Id: id(p)
                                                              , Name: p.Name
                                                              , Description: p.Description
                                                              , Category:{Id: id(c), Name: c.Name }}"));
                var result = await query.ResultsAsync;
                return result.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw (e);
            }

        }

        public async Task<int> DeleteProduct(int id)
        {
            try
            {
                var query = _graphClient.Cypher.Read
                        .Match($"(c:Category)-[r:HAS_PRODUCT]->(p:Product)")
                        .Where($"id(p)={id}")
                        .Delete("r")
                        .Merge("(c)-[:HAD_PRODUCT]->(p)");
                await query.ExecuteWithoutResultsAsync();
                return id;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public async Task<IEnumerable<ProductDto>> GetProduct()
        {
            try
            {
                var query = _graphClient.Cypher.Read
                        .Match($"(c:Category)-[:HAS_PRODUCT]->(p:Product)")
                        .With("c, p")
                        .Return(_ => Return.As<ProductDto>(@"{Id: id(p)
                                                              , Name: p.Name
                                                              , Description: p.Description
                                                              , Category:{Id: id(c), Name: c.Name }}"));
                var result = await query.ResultsAsync;
                return result.OrderBy(x=>x.Category.Id);
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public async Task<ProductDto> GetProductById(int id)
        {
            try
            {
                var query = _graphClient.Cypher.Read
                        .Match($"(c:Category)-[:HAS_PRODUCT]->(p:Product)")
                        .Where($"id(p)={id}")
                        .With("c,p")
                        .Return(_ => Return.As<ProductDto>(@"{  Id: id(p)
                                                              , Name: p.Name
                                                              , Description: p.Description
                                                              , Category:{Id: id(c), Name: c.Name }}"));
                var result = await query.ResultsAsync;
                return result.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public async Task<ProductDto> UpdateProduct(ProductDto dto)
        {
            try
            {
                var query = _graphClient.Cypher.Read
                        .Match($"(c:Category)-[:HAS_PRODUCT]->(p:Product)")
                        .Where($"id(p)={dto.Id}")
                        .With("c,p")
                        .Set($"p.Description = '{dto.Description}',p.Name='{dto.Name}'")
                        .Return(_ => Return.As<ProductDto>(@"{Id: id(p)
                                                              , Name: p.Name
                                                              , Description: p.Description
                                                              , Category:{Id: id(c), Name: c.Name }}"));
                var result = await query.ResultsAsync;

                ProductDto productDto = result.FirstOrDefault();

                if (productDto.Category.Id != dto.Category.Id)
                {
                    var query1 = _graphClient.Cypher.Read
                        .Match($"(c1:Category)-[r:HAS_PRODUCT]->(p:Product)")
                        .Where($"id(p)={dto.Id}")
                        .Match($"(c:Category)")
                        .Where($"id(c)={dto.Category.Id}")
                        .With("c,p,r")
                        .Delete("r")
                        .Create("(c)-[:HAS_PRODUCT]->(p)")
                        .Return(_ => Return.As<ProductDto>(@"{Id: id(p)
                                                              , Name: p.Name
                                                              , Description: p.Description
                                                              , Category:{Id: id(c), Name: c.Name }}"));
                    var result1 = await query1.ResultsAsync;
                    return result1.FirstOrDefault();
                }
                return result.FirstOrDefault();
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }
}
