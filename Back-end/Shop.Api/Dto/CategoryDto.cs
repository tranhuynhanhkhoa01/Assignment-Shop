﻿using Newtonsoft.Json;
using System.Collections;
using System.Text.Json.Serialization;

namespace Shop.Api.Dto
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
