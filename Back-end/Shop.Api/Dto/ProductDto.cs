﻿namespace Shop.Api.Dto
{
    public class ProductDto
    {
        public ProductDto()
        {
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public CategoryDto Category { get; set; }
    }
}
