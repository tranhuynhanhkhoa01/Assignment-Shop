﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Api.Models
{
    public class Neo4jSetting
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string BoltUrl { get; set; }
    }
}
