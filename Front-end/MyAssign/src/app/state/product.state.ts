import { Product } from "../Models/Product";

export interface IProductState {
    products: Product[]
}