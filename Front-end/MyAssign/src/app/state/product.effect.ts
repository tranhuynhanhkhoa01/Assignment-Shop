import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ProductsService } from "../products.service";
import { Product } from "../Models/Product";
import { EProductActions, GetProductListAction, ProductSuccessAction, ProductFailedAction, AddProductAction, UpdateProductAction, DeleteProductAction } from "./product.action";


@Injectable()
export class ProductEffects {

    constructor(private actions$: Actions, private productService: ProductsService) {
    }



    @Effect()
    getListProduct$ = this.actions$.pipe(
        ofType(EProductActions.GET_PRODUCT_LIST),
        switchMap((action: GetProductListAction) => {
            return this.productService.getProducts().pipe(
                map(
                    heroList => new ProductSuccessAction(
                        EProductActions.GET_PRODUCT_LIST,
                        heroList
                    )
                ),
                catchError(err =>
                    Observable.of(new ProductFailedAction(EProductActions.ACTION_FAILED, err))
                )
            );
        })
    );

    @Effect()
    addProduct$ = this.actions$.pipe(
        ofType(EProductActions.ADD_PRODUCT),
        switchMap((action: AddProductAction) => {
            return this.productService.addProduct(action.payload as Product).pipe(
                map(
                    hero => new ProductSuccessAction(
                        EProductActions.ADD_PRODUCT,
                        hero
                    )
                ),
                catchError(err =>
                    Observable.of(new ProductFailedAction(EProductActions.ACTION_FAILED, err))
                )
            );
        })
    );

    @Effect()
    updateProduct$ = this.actions$.pipe(
        ofType(EProductActions.UPDATE_PRODUCT),
        switchMap((action: UpdateProductAction) => {
            return this.productService.updateProduct(action.payload as Product).pipe(
                map(
                    hero => new ProductSuccessAction(
                        EProductActions.UPDATE_PRODUCT,
                        hero
                    )
                ),
                catchError(err =>
                    Observable.of(new ProductFailedAction(EProductActions.ACTION_FAILED, err))
                )
            );
        })
    );

    @Effect()
    deleteProduct$ = this.actions$.pipe(
        ofType(EProductActions.DELETE_PRODUCT),
        switchMap((action: DeleteProductAction) => {
            return this.productService.deleteProduct(action.payload as number).pipe(
                map(
                    id => new ProductSuccessAction(
                        EProductActions.DELETE_PRODUCT,
                        id
                    )
                ),
                catchError(err =>
                    Observable.of(new ProductFailedAction(EProductActions.ACTION_FAILED, err))
                )
            );
        })
    );
};