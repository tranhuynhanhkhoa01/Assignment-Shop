import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store, createSelector, createFeatureSelector } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { BaseSelector } from '../base.selector';
import { Product } from '../Models/Product';
import { ProductSuccessAction, ProductFailedAction, EProductActions } from './product.action';

@Injectable()
export class ProductSelectors extends BaseSelector {
  public product$: Observable<Product[]>;
  public actionSuccess$: Observable<ProductSuccessAction>;
  public actionFail$: Observable<ProductFailedAction>;

  constructor(
      private store: Store<any>,
      private heroActions: Actions
  ) {
    super(heroActions, EProductActions.ACTION_SUCCESS, EProductActions.ACTION_FAILED);

    this.product$ = this.store.select(x => x.app.products);

    this.actionSuccess$ = this.heroActions.ofType(
      EProductActions.ACTION_SUCCESS
    );

    this.actionFail$ = this.heroActions.ofType(
      EProductActions.ACTION_FAILED
    );
  }
}
