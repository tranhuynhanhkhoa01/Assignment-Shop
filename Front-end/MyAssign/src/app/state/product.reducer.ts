import { IProductState } from './product.state';
import { ProductAction, EProductActions, ProductSuccessAction, ProductFailedAction } from './product.action';
import { Product } from '../Models/Product';

export const initProductState: IProductState = {
  products: []
}

export function productReducer(heroState: IProductState = initProductState, action: ProductAction): IProductState {
  switch (action.type) {
    case EProductActions.ACTION_SUCCESS:
      return actionSuccessReducer(heroState, action as ProductSuccessAction);
    case EProductActions.ACTION_FAILED:
      return actionFailedReducer(heroState, action as ProductFailedAction);
    default:
      return heroState;
  }
}

function actionSuccessReducer(productState: IProductState, action: ProductSuccessAction): IProductState {
  switch (action.subType) {

    case EProductActions.ADD_PRODUCT: {
      return {
        products: [...productState.products, action.payload as Product]
      };
    }
    case EProductActions.UPDATE_PRODUCT: {
      return {
        products: updateItem([...productState.products], action.payload as Product)
      }
    }
    case EProductActions.DELETE_PRODUCT: {
      return {
        products: deleteItem([...productState.products], action.payload as number)
      }
    }
    // case EProductActions.GET_PRODUCT_DETAIL: {
    //   return {
    //     heroes: [...heroState.heroes]
    //   };
    // }
    case EProductActions.GET_PRODUCT_LIST: {
      return {
        products: [...action.payload] as Product[]
      };
    }

    default:
      return productState;
  }
}

function actionFailedReducer(productState: IProductState, action: ProductFailedAction): IProductState {
  switch (action.subType) {
    default:
      return productState;
  }
}

function updateItem(array: Product[], hero: Product): Product[] {
  array.forEach(element => {
    if (element.id == hero.id) {
      element.name = hero.name;
    }
  });
  return array
}

function deleteItem(array: Product[], id: number): Product[] {
  array = array.filter(item => item.id !== id);
  return array
}