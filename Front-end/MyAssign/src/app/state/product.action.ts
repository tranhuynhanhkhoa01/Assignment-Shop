import { Action } from '@ngrx/store';
import { BaseSuccessAction, BaseFailedAction } from '../action.interface';
import { Product } from '../Models/Product';

export enum EProductActions {
    GET_PRODUCT_LIST = '[HERO] Get List Product',
    GET_PRODUCT_DETAIL = '[HERO] Get Detail Product',
    ADD_PRODUCT = '[USER] Add Product',
    UPDATE_PRODUCT = '[USER] Update Product',
    DELETE_PRODUCT = '[USER] Delete Product',
    ACTION_SUCCESS = '[PRODUCT] Action Success',
    ACTION_FAILED = '[PRODUCT] Action Failed'
}

export interface ProductAction extends Action {
    type: EProductActions;
    payload?: any;
}

export class GetProductListAction implements ProductAction {
    type = EProductActions.GET_PRODUCT_LIST;
    constructor() { }
}
export class GetProductDetailAction implements ProductAction {
    type = EProductActions.GET_PRODUCT_DETAIL;
    constructor(public payload: number) { }
}

export class AddProductAction implements ProductAction {
    type = EProductActions.ADD_PRODUCT;
    constructor(public payload: Product) { }
}

export class UpdateProductAction implements ProductAction {
    type = EProductActions.UPDATE_PRODUCT;
    constructor(public payload: Product) { }
}

export class DeleteProductAction implements ProductAction {
    type = EProductActions.DELETE_PRODUCT;
    constructor(public payload: Number) { }
}

export class ProductSuccessAction implements BaseSuccessAction {
    type = EProductActions.ACTION_SUCCESS;
    constructor(public subType: EProductActions, public payload: any) { }
}

export class ProductFailedAction implements BaseFailedAction {
    type = EProductActions.ACTION_FAILED;
    constructor(public subType: EProductActions, public payload: any) { }
}