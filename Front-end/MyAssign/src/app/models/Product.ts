import { Category } from "./Category";

export class Product {
    id: number;
    name: string;
    description: string;
    category: Category;
}