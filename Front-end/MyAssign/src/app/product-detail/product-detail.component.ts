import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../Models/Product';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../products.service';
import { Location } from '@angular/common';
import { Category } from '../Models/Category';
import { Route } from '@angular/compiler/src/core';
import { Store } from '@ngrx/store';
import { AddProductAction, UpdateProductAction } from '../state/product.action';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product: Product = { id: 0, name: "", description: "", category: { id: 0, name: "" } };
  category: Category[];
  isCreate: boolean = true;
  productId: number;

  constructor(
    private productService: ProductsService,
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private strore: Store<any>) { }

  ngOnInit(): void {
    this.productId = +this.route.snapshot.paramMap.get('id');
    this.productService.getCateory().subscribe(category => this.category = category);
    if (this.productId != 0) {
      this.isCreate = false;
      this.productService.getProduct(this.productId).subscribe(product => {
        this.product.id = product.id,
          this.product.name = product.name,
          this.product.description = product.description,
          this.product.category.id = product.category.id,
          this.product.category.name = product.category.name
      });
    }
  }

  update(): void {
    // this.productService.updateProduct(this.product).subscribe(product => {
    //   this.product.id = product.id,
    //     this.product.name = product.name,
    //     this.product.description = product.description,
    //     this.product.category.id = product.category.id,
    //     this.product.category.name = product.category.name
    // });
    this.strore.dispatch(new UpdateProductAction(this.product));
  }

  create(): void {
    // this.productService.addProduct(this.product).subscribe(product => {
    //   this.product.id = product.id,
    //     this.product.name = product.name,
    //     this.product.description = product.description,
    //     this.product.category.id = product.category.id,
    //     this.product.category.name = product.category.name
    // });

    this.strore.dispatch(new AddProductAction(this.product));

  }

  goBack(): void {
    this.location.back();
  }

}
