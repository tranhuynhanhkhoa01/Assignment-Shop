import { Component, OnInit } from '@angular/core';
import { Product } from '../Models/Product';
import { ProductsService } from '../products.service';
import { Router } from '@angular/router';
import { ProductSelectors } from '../state/product.selectors';
import { Store } from '@ngrx/store';
import { GetProductListAction, DeleteProductAction } from '../state/product.action';

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[];

  constructor(private productService: ProductsService,
    private productSelectors: ProductSelectors,
    private store: Store<any>) { }

  ngOnInit() {
    this.getProducts();
    this.registerSubcriptions();
  }

  getProducts() {
    this.store.dispatch(new GetProductListAction());
  }

  deleteProduct(product: Product) {
    this.store.dispatch(new DeleteProductAction(product.id));
  }

  registerSubcriptions() {
    this.productSelectors.product$.subscribe(
      x => this.products = x
    )
  }

}
