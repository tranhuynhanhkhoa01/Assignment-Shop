import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './Models/Product';
import { catchError, map, tap } from 'rxjs/operators';
import { Category } from './Models/Category';

@Injectable()
export class ProductsService {

  private ProductUrl = 'https://localhost:44306/api/product';  // URL to web apihttps://localhost:44306/api/product
  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http:HttpClient) { }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.ProductUrl).pipe(
    );
  }

  getProduct(id: number): Observable<Product> {
    const url = `${this.ProductUrl}/${id}`;
    return this.http.get<Product>(url).pipe(
    );
  }

  getCateory(): Observable<Category[]> {
    const url = "https://localhost:44306/api/category";
    return this.http.get<Category[]>(url).pipe(
    );
  }


  updateProduct(product: Product): Observable<Product> {
    return this.http.put<Product>(this.ProductUrl, product, this.httpOptions).pipe(
      // tap(_ => this.log(`updated hero id=${hero.id}`)),
      // catchError(this.handleError<any>('updateHero'))
    );
  }

  addProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.ProductUrl, product, this.httpOptions).pipe(
    );
  }

  deleteProduct(p: Product |number ): Observable<any> {
    const id = typeof p === 'number' ? p : p.id;
    const url = `${this.ProductUrl}/${id}`;
  
    return this.http.delete<Product>(url, this.httpOptions).pipe(
      // tap(_ => this.log(`deleted hero id=${id}`)),
      // catchError(this.handleError<Hero>('deleteHero'))
    );
  }

  // searchHeroes(term: string): Observable<Hero[]> {
  //   if (!term.trim()) {
  //     // if not search term, return empty hero array.
  //     return Observable.of([]);
  //   }
  //   return this.http.get<Hero[]>(`${this.heroesUrl}/?name=${term}`).pipe(
  //     tap(x => x.length ?
  //        this.log(`found heroes matching "${term}"`) :
  //        this.log(`no heroes matching "${term}"`)),
  //     catchError(this.handleError<Hero[]>('searchHeroes', []))
  //   );
  // }

  // /** Log a HeroService message with the MessageService */
  // private log(message: string) {
  //   this.messageService.add(`HeroService: ${message}`);
  // }

  // private handleError<T>(operation = 'operation', result?: T) {
  //   return (error: any): Observable<T> => {

  //     console.error(error); // log to console instead

  //     this.log(`${operation} failed: ${error.message}`);

  //     // Let the app keep running by returning an empty result.
  //     return Observable.of(result as T);
  //   }

  // }

}
