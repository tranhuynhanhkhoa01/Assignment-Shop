import { Actions } from '@ngrx/effects';
import { BaseSuccessAction, BaseFailedAction } from './action.interface';
import { Observable } from 'rxjs/Observable';
import { filter } from 'rxjs/operators/filter';
import { map } from 'rxjs/operators/map';

export class BaseSelector {
  constructor(
    protected actions: Actions,
    protected actionSuccessType: string,
    protected actionFailedType: string
  ) { }

  actionSuccessOfSubtype$ = (...typeNames: string[]) =>
    this.actions.ofType(this.actionSuccessType)
      .map(action => action as BaseSuccessAction)
      .filter(a => typeNames.includes(a.subType))


  actionFailedOfSubtype$ = (...typeNames: string[]) =>
    this.actions.ofType(this.actionFailedType)
      .map(action => action as BaseFailedAction)
      .filter(a => typeNames.includes(a.subType))

  actionOfType$ = (...typeNames: string[]) =>
    this.actions
      .map(action => action as BaseSuccessAction)
      .filter(a => typeNames.includes(a.type))

  readonly loading$ = (typeName: string): Observable<boolean> =>
    this.actions.pipe(
      filter(action => (action['subType'] || action.type) === typeName),
      map(action => action.type === typeName)
    )
}
