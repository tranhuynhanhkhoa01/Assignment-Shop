import { Action } from '@ngrx/store';

export interface BaseSuccessAction extends Action{
    payload: any,
    subType: string
}

export interface BaseFailedAction extends Action{
    payload: any,
    subType: string
}